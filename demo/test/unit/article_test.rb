require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
	def test_save_without_title
		article = Article.new(:body => 'new body')
		assert_equal article.valid?, false
    assert_equal article.save, false
	end
  
  def test_save_with_title_and_body
    article = Article.new(:title => 'testing', :body => 'body testing')
    assert_equal article.valid?, true
    assert_equal article.save, true
  end
  

  def test_relation_between_article_and_comment
  user = User.create({ :first_name => 'endah', :last_name => 'ratri', :password_hash => '$2a$10$DGaqM6BmwK4QpOgIsNR2a.5uNnRL5qeExI5SQ1ycCR.wkaRXlcczy',:password_salt => '$2a$10$DGaqM6BmwK4QpOgIsNR2a.', :email => "endah@gmail.com"})
  assert_not_nil user
  article = Article.create({:title => 'new title', :body => 'new body', :user_id => user.id})
  assert_not_nil article
  assert_not_nil user.articles
  comment = Comment.create(:content => 'new comment', :article_id => article.id)
  assert_not_nil article.comments
  assert_equal article.comments.empty?, false
  assert_equal article.comments[0].class, Comment
  end

  def test_relation_between_user_and_articles
  article = Article.create({:title => 'new title', :body => 'new body', :user_id => User.first.id})
  assert_not_nil User.first.articles
  assert_equal User.first.articles.empty?, false
  assert_equal User.first.articles[0].class, Article
  end

  def test_filter_rating_is_and_above
  article = Article.rating_is_and_above(2)
  assert_not_nil article
  end
end
