require 'test_helper'

class UserTest < ActiveSupport::TestCase

	def test_save_without_password
		user = User.new(:first_name => 'endah', :last_name => 'ratri', :email => 'endah@gmail.com')
		assert_equal user.valid?, false
    assert_equal user.save, false
	end

  def test_save_without_email
		user = User.new(:first_name => 'endah', :last_name => 'ratri', :password => 'adghjbn')
		assert_equal user.valid?, false
    assert_equal user.save, false
	end
  
  def test_save_without_password_and_email
		user = User.new(:first_name => 'endah', :last_name => 'ratri' )
		assert_equal user.valid?, false
    assert_equal user.save, false
	end

  def test_save_same_email
		user1 = User.create(:first_name => 'endah', :last_name => 'ratri', :password => 'anana', :email => 'endahaaa@gmail.com')
    assert_equal user1.valid?, true 
    assert_equal user1.save, true
    user2 = User.create(:first_name => 'puspita', :last_name => 'ratri', :password => 'aaaa', :email => 'endahaaa@gmail.com')
		assert_equal user2.valid?, false
    assert_equal user2.save, false
  end

  def test_new_user_valid
  user = User.create({ :first_name => 'endah', :last_name => 'ratri', :password => 'bcvbbnm', :email => "endaah@gmail.com"})
    assert_equal user.valid?, true 
    assert_equal user.save, true
  end
  
 def test_my_country_articles
    user = User.first
    article = Article.create({:title => 'new title', :body => 'my country body', :user_id => user.id})
    assert_not_nil user.articles
    assert_not_nil user.my_country_articles
    assert_equal user.my_country_articles[0].body, "my country body"
  end

  def test_relation_between_user_and_articles
  article = Article.create({:title => 'new title', :body => 'new body', :user_id => User.first.id})
  assert_not_nil User.first.articles
  assert_equal User.first.articles.empty?, false
  assert_equal User.first.articles[0].class, Article
  end

  def test_relation_between_user_and_product
    user = User.first
    product = Product.create({:name => 'new product', :price => '100000', :user_id => user.id})
    assert_not_nil user.products
    assert_equal user.products.empty?, false
    assert_equal user.products[0].class, Product
  end

  def test_relation_between_user_and_country
    country = Country.first
    user = User.create({:first_name => 'endah', :last_name =>'ratri', :password => 'mypass', :email => 'endaah.ratri@kiranatama.com', :country_id => country.id}) 
	  assert_not_nil country.users
    assert_equal country.users.empty?, false
    assert_equal country.users[0].class, User
  end

  def test_full_address
    user = User.first
    assert_equal user.full_address, "Bandung Indonesia"
  end

  def test_authenticate
    email = 'Steve@buble.com'
    password = 'a'
    User.authenticate(email, password)
    assert_not_nil :user
  end
  
  def test_false_authenticate
    email = 'Steve@buble.com'
    password = 'b'
    User.authenticate(email, password)

  end
end
