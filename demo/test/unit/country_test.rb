require 'test_helper'

class CountryTest < ActiveSupport::TestCase
  def test_save_without_code
		country = Country.new(:name => 'Bangladesh')
		assert_equal country.valid?, false
    assert_equal country.save, false
	end
 
  def test_save_without_name
		country = Country.new(:code => 'id')
		assert_equal country.valid?, false
    assert_equal country.save, false
	end
  
  def test_save_valid_code
		country = Country.new(:code => 'id', :name => 'Indonesia')
		assert_equal country.valid?, true
    assert_equal country.save, true
	end
  
  def test_save_invalid_code
		country = Country.new(:code => 'idn', :name => 'Indonesia')
		assert_equal country.valid?, false
    assert_equal country.save, false
	end
  
  def test_relation_between_user_and_country
    country = Country.create({:id => 4, :code => 'frc', :name => 'France'})
    user = User.create({:first_name => 'endah', :last_name =>'ratri', :password => 'mypass', :email => 'endaah.ratri@kiranatama.com', :country_id => country.id}) 
	  assert_not_nil country.users
    assert_equal country.users.empty?, false
    assert_equal country.users[0].class, User
  end
end
