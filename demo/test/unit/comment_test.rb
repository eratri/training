require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  def test_relation_between_article_and_comment
  user = User.first
  article = Article.create({:title => 'new title', :body => 'new body', :user_id => user.id})
  assert_not_nil article
  assert_not_nil user.articles
  comment = Comment.create(:content => 'new comment', :article_id => article.id)
  assert_not_nil article.comments
  assert_equal article.comments.empty?, false
  assert_equal article.comments[0].class, Comment
  end
end
