require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  def test_relationship_between_category_and_product
	  category = Category.create({:id => 1, :name => 'food'})
    product = Product.create({:id => 1, :name => 'donuts', :price =>'2000', :user_id => User.first.id})
    categories_product = CategoriesProduct.create(:id => 1, :category_id => category.id, :product_id => product.id)
  	assert_not_nil category.categories_products
    assert_not_nil product.categories_products
	  assert_equal category.categories_products.empty?, false
	  assert_equal product.categories_products.empty?, false
		assert_equal category.categories_products[0].class, CategoriesProduct
    assert_equal product.categories_products[0].class, CategoriesProduct
  end
end
