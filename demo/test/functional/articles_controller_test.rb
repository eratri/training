require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  def setup
		@article = Article.find(:first)
  end
 
	def test_index
		get :index
    assert_response :success
		assert_not_nil assigns(:articles)
  end
  
  def test_new
    login_as('endah@gmail.com')
		get :new
    assert_response :success
		assert_not_nil assigns(:article)
  end

  def test_create
    login_as('endah@gmail.com')
      assert_difference('Article.count') do
      post :create, :article => {:title => 'new title', :body => "new body"}
      assert_not_nil assigns(:article)
      assert_equal assigns(:article).title, "new title"
      assert_equal assigns(:article).valid?, true
    end
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:notice], 'New Article Added ... '
  end
  
  def test_create_with_invalid_parameter
    login_as('endah@gmail.com')
    assert_no_difference('Article.count') do
      post :create, :article => {:title => nil, :body => nil}
      assert_not_nil assigns(:article)
      assert_equal assigns(:article).valid?, false
    end
    assert_response :success
  end
  
  def test_edit
    login_as('Steve@buble.com')
    get :edit, :id => Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end
  
  def test_update
    login_as('Steve@buble.com')
    put :update, :id => Article.first.id,
                 :article => {:title => 'updated title', :body => "updated body"}
    assert_not_nil assigns(:article)
          assert_equal assigns(:article).title, 'updated title'
           assert_response :redirect
    assert_redirected_to articles_path
        assert_equal flash[:notice], 'Article Updated !' 
  end
  
  def test_update_with_invalid_parameter
    login_as('Steve@buble.com')
    put :update, :id => Article.first.id,
                 :article => {:title => nil, :body => nil}
    assert_not_nil assigns(:article)
    assert_response :success
  end
  
  def test_destroy
    login_as('Steve@buble.com')
    assert_difference('Article.count', -1) do
    delete :destroy, :id => Article.first.id
    assert_not_nil assigns(:article)
    end
    assert_response :redirect
    assert_redirected_to articles_path
  end
  
  def test_show
    login_as('Steve@buble.com')
    get :show, :id => Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end

end
