require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  def test_index
		get :index
    assert_response :success
		assert_not_nil assigns(:comments)
  end
  
  def test_new
		get :new
    assert_response :success
		assert_not_nil assigns(:comment)
  end

  def test_create
    login_as('endah@gmail.com')
    assert_difference('Comment.count') do
      post :create, :comment => {:content => "new content", :article_id => Article.first.id}
      assert_not_nil assigns(:comment)
      assert_equal assigns(:comment).content, "new content"
      assert_equal assigns(:comment).valid?, true
    end
    assert_response :redirect
    assert_redirected_to article_path(Article.first.id)
    assert_equal flash[:notice], 'Comment was successfully created.'
  end
end
