class CommentsController < ApplicationController

  def index
    @comments = Comment.all
  end
  
  def new
    @comment = Comment.new
  end

  def create
    @comment = Comment.new(params[:comment]) 
     respond_to do |format|
      if @comment.save
        format.html { redirect_to(article_path(@comment.article_id), :notice => 'Comment was successfully created.') }
      format.js
     end
     end

    #if @comment.save 
    #  flash[:notice] = 'New Comment Added ... ' 
    #  redirect_to(article_path(@comment.article_id))
    #else 
    #  render "new" 
    #end  
  end

end
