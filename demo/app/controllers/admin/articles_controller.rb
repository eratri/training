class Admin::ArticlesController < Admin::ApplicationController
  before_filter :require_admin_login
  before_filter :find_article_by_id, :only => [:edit, :update, :destroy]

  def index
    @admin_articles = Article.all
  end

  def new
    @admin_article = Article.new
  end

  def create
    @admin_article = Article.new(params[:article]) 
    if @admin_article.save 
      flash[:notice] = 'New Article Added ... ' 
      redirect_to admin_articles_path
    else 
      render "new"
    end  
  end

  def edit
	end
  
  def update
  	if @admin_article.update_attributes(params[:article])
      flash[:notice] = 'Article Updated !' 
      redirect_to admin_articles_path
    else
      render "edit"
    end
  end

  def destroy
    @admin_article.destroy
    redirect_to admin_articles_path
  end

	#find article by id
  def find_article_by_id
    @admin_article = Article.find(params[:id])
  end
end
