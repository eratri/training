class ArticlesController < ApplicationController
   before_filter :require_login, :only => [:new, :create, :edit, :update, :delete]
   before_filter :find_article_by_id, :only => [:edit, :update, :destroy, :show]
 
  def index
    @articles = Article.all
  end
  
  def new
    @article = Article.new
  end

  def create
    @article = Article.new(params[:article]) 
    if @article.save 
      flash[:notice] = 'New Article Added ... ' 
      redirect_to articles_path 
    else 
      render "new" 
    end  
  end
  
  def edit; end
  
  def update
   if @article.update_attributes(params[:article])
     flash[:notice] = 'Article Updated !' 
     redirect_to articles_path
   else
     render :action => :edit
   end
  end

  def destroy
    @article.destroy
    redirect_to articles_path
  end

  def show
    @comments = @article.comments
    @comment = Comment.new
  end

  def find_article_by_id
    @article = Article.find(params[:id])
    if session[:user_id] != @article.user_id
       flash[:notice] = 'You are not authenticated user ... '
       redirect_to :action => :index
    end
  end

end
