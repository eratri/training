class User < ActiveRecord::Base
  has_many :products, :dependent => :destroy #plural, products not product
  has_many :articles, :dependent => :destroy
  has_many :my_country_articles,
             :class_name => "Article",
             :foreign_key => "User_id",
             :conditions => "body like '%my country%'"
  belongs_to :country

  attr_accessible :first_name, :last_name, :country_id, :email, :password, :password_confirmation 
  attr_accessor :password
  before_save :encrypt_password

  validates :password, :presence => {:on => :create}, :confirmation => true
  #validates :first_name, :length => {:maximum => 20}, :format => {:with => /[a-zA-Z\s]+$/}
  validates :email, :presence => true, :uniqueness => true, :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
  
  #validation encrypt password
  def encrypt_password
    if password.present?
          self.password_salt = BCrypt::Engine.generate_salt
          self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
     end
  end

  #method show address+country
  def full_address
    "#{self.address} #{self.country.name}" #simple way...
  end
  
  #method authentication login
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
end
