class Product < ActiveRecord::Base
  belongs_to :user
  has_many :categories, :through => :categories_product
  has_many :categories_products
  scope :price_more_than_1000, where("price > '1000'")
end
