class Country < ActiveRecord::Base
  has_many :users
  validates :code, :presence => true
  validates :name, :presence => true, :length => {:maximum => 15}
  validate :valid_code

  #valid country
  def valid_code
    self.errors[:code] << "only id, use, frc" unless code == 'id' or code == 'usa' or code == 'frc'
    #self.errors[:code] << "only id, use, frc" unless ['id', 'usa', 'frc'].include?("#(code)")
  end  
end
