# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = User.create([ {:first_name => "Martin", :last_name => "No Name"}, {:first_name => "John", :last_name => "PragProg"} ]) 

articles = Article.create([ {:title => "first article", :body => "first body content"}, {:title => "second article", :body => "second body content"}, {:title => "third article", :body => "third body content"}, {:title => "fourth article", :body => "fourth body content"}, {:title => "fifth article", :body => "fifth body content"} ])

comments = Comment.create([ {:content => "Asadasfdf"}, {:content => "1231414"}, {:content => "Test"}])

countries = Country.create([ {:code => "IDN", :name => "Indonesia"}, {:code => "ENG", :name => "England"} ])

