class DropTableUser < ActiveRecord::Migration
  def up
    drop_table :user
  end

  def down
  end
end
