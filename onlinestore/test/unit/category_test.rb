require 'test_helper'

class CategoryTest < ActiveSupport::TestCase

	def test_save_without_name
   category = Category.create( )
    assert_equal category.valid?, false
    assert_equal category.save, false
  end

  def test_category_tree_parent
    category = Category.first
    assert_equal category.category_tree, "Automotive"
  end

  def test_category_tree_child
    category = Category.find_by_id(2)
    assert_equal category.category_tree, "__ Automotive : Car"
  end
end
