require 'test_helper'

class ArticleTest < ActiveSupport::TestCase

  def test_relation_between_article_and_comment
   article = Article.first
   comment = Comment.create(:comment => "my comment", :article_id => article, :user_id => User.first.id )
   assert_not_nil article.comments
   assert_equal article.comments.empty?, false
   assert_equal article.comments[0].class, Comment
 end

  def test_relation_between_article_and_user
   user = User.first
    article = Article.new(:title => "Testing", :body => "This is body", :user_id => user)
   assert_not_nil user.articles
   assert_equal user.articles.empty?, false
   assert_equal user.articles[0].class, Article
 end
  def test_save_without_title
    article = Article.new(:body => 'new_body')
    assert_equal article.valid?, false
    assert_equal article.save, false
  end

  def test_save_without_body
    article = Article.new(:title => 'new title')
    assert_equal article.valid?, false
    assert_equal article.save, false
  end

  def test_save_with_title_and_body
    article = Article.new(:title => "Testing", :body => "This is body", :user_id => User.first.id)
    assert_equal article.valid?, true
    assert_equal article.save, true
 end

end
