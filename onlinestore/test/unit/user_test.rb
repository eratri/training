require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def test_authenticate
    email = 'admin@admin.com'
    password = 'admin'
    User.authenticate(email, password)
    assert_not_nil :user
  end

	def test_save_without_name
   user = User.create(:email => "endah.p.ratri@gmail.com", :password => "endah")
    assert_equal user.valid?, false
    assert_equal user.save, false
  end

	def test_save_without_email
   user = User.create(:name => "endah", :password => "endah")
    assert_equal user.valid?, false
    assert_equal user.save, false
  end

	def test_save_without_password
   user = User.create(:email => "endah.p.ratri@gmail.com", :name => "endah")
    assert_equal user.valid?, false
    assert_equal user.save, false
  end

	def test_save_with_name_email_and_password
   user = User.create(:name => "endah", :email => "endah.p.ratri@gmail.com", :password => "endah")
    assert_equal user.valid?, true
    assert_equal user.save, true
  end

	def test_save_with_same_email
   user = User.create(:name => "endah", :email => "endah.p.ratri@gmail.com", :password => "endah")
    assert_equal user.valid?, true
    assert_equal user.save, true
   user_next = User.create(:name => "puspita", :email => "endah.p.ratri@gmail.com", :password => "endah")
    assert_equal user_next.valid?, false
    assert_equal user_next.save, false
  end
end
