require 'test_helper'

class CommentTest < ActiveSupport::TestCase
 def test_relation_between_comment_and_user
   user = User.first
   comment = Comment.create(:comment => "my comment", :article_id => Article.first.id, :user_id => user )
   assert_not_nil user.comments
   assert_equal user.comments.empty?, false
   assert_equal user.comments[0].class, Comment
 end


  def test_save_without_comment
    comment = Comment.new(:article_id => Article.first.id, :user_id => User.first.id)
    assert_equal comment.valid?, false
    assert_equal comment.save, false
  end

  def test_save_with_comment
    comment = Comment.new(:comment => "my comment", :article_id => Article.first.id, :user_id => User.first.id)
    assert_equal comment.valid?, true
    assert_equal comment.save, true
  end
end
