require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  def test_relation_between_product_and_user
   user = User.first
   category = Category.last
   product = Product.create(:name => "donuts", :price => "1000", :user_id => user, :category_id => category)
   assert_not_nil user.products
   assert_equal user.products.empty?, false
   assert_equal user.products[0].class, Product
 end

  def test_relation_between_product_and_category
   category = Category.first
   product = Product.create(:name => "toyota kijang", :price => "1000000", :user_id => User.first.id, :category_id => category)
   assert_not_nil category.products
   assert_equal category.products.empty?, false
   assert_equal category.products[0].class, Product
 end

	 def test_save_without_name
   product = Product.create(:price => "1000000", :user_id => User.first.id, :category_id => Category.first.id)
    assert_equal product.valid?, false
    assert_equal product.save, false
  end

 def test_save_without_price
   product = Product.create(:name => "newest automotive", :user_id => User.first.id, :category_id => Category.first.id)
    assert_equal product.valid?, false
    assert_equal product.save, false
  end
 def test_save_with_name_and_price
   product = Product.create(:name => "newest automotive", :price=> "1000000000", :user_id => User.first.id, :category_id => Category.first.id)
    assert_equal product.valid?, true
    assert_equal product.save, true
  end
end
