require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  def setup
		@product = Product.find(:first)
  end
 
	def test_index
		get :index
    assert_response :success
		assert_not_nil assigns(:products)
  end
  
  def test_new
    login_as('endah@gmail.com')
		get :new
    assert_response :success
		assert_not_nil assigns(:product)
		assert_not_nil assigns(:category)
  end

 def test_create
    login_as('endah@gmail.com')
    assert_difference('Product.count') do
      post :create, :product => {:name => 'new product', :price => "1000000", :user_id => session[:user_id], :category_id => Category.first.id}
      assert_not_nil assigns(:product)
      assert_equal assigns(:product).name, "new product"
      assert_equal assigns(:product).valid?, true
    end
    assert_response :redirect
    assert_redirected_to products_path
    assert_equal flash[:notice], 'New Product Added ... '
  end
  
  def test_create_with_invalid_parameter
    login_as('endah@gmail.com')
    assert_no_difference('Product.count') do
      post :create, :product => {:name => nil, :price => nil}
      assert_not_nil assigns(:product)
      assert_not_nil assigns(:category)
      assert_equal assigns(:product).valid?, false
    end
    assert_response :success
  end
  
  def test_edit
    login_as('admin@admin.com')
    get :edit, :id => Product.first.id
    assert_not_nil assigns(:product)
    assert_response :success
  end
  
  def test_update
    login_as('admin@admin.com')
    put :update, :id => Product.first.id,
                 :product => {:name => 'updated new product'}
    assert_not_nil assigns(:product)
    assert_equal assigns(:product).name, 'updated new product'
    assert_response :redirect
    assert_redirected_to product_path(assigns(:product))
    assert_equal flash[:notice], 'Product Updated !' 
  end
  
  def test_update_with_invalid_parameter
    login_as('admin@admin.com')
    put :update, :id => Product.first.id,
                 :product => {:name => nil}
    assert_not_nil assigns(:product)
    assert_not_nil assigns(:category)
    assert_response :success
  end
  
  def test_destroy
    login_as('admin@admin.com')
    assert_difference('Product.count', -1) do
      delete :destroy, :id => Product.first.id
      assert_not_nil assigns(:product)
    end
    assert_response :redirect
    assert_redirected_to products_path
  end
  
  def test_show
    login_as('admin@admin.com')
    get :show, :id => Product.first.id
    assert_not_nil assigns(:product)
    assert_response :success
  end

  def test_authenticate_edit
    login_as('endah@gmail.com')
		assert_no_difference('Product.count', -1) do
      delete :destroy, :id => Product.first.id
      assert_not_nil assigns(:product)
    end
    assert_response :redirect
    assert_redirected_to products_path
  end

  def test_list
    login_as('endah@gmail.com')
    get :list, :id => Category.first.id
    assert_not_nil assigns(:products)
    assert_not_nil assigns(:category)
    assert_response :success
  end

  def test_list_without_id
    login_as('endah@gmail.com')
    get :list, :id => nil
    assert_response :redirect
    assert_redirected_to products_path
  end
end
