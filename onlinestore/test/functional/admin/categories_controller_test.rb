require 'test_helper'

class Admin::CategoriesControllerTest < ActionController::TestCase
  def setup
		@category = Category.find(:first)
  end
  
  def test_index
    login_as('admin@admin.com')
		get :index
    assert_response :success
		assert_not_nil assigns(:categories)
  end

	def test_index_not_admin
    login_as('endah@gmail.com')
		get :index
		assert_response :redirect
    assert_redirected_to root_url
    assert_equal flash[:error], 'Only admins are permitted'
  end

	def test_index_not_login
		get :index
		assert_response :redirect
    assert_redirected_to log_in_path
    assert_equal flash[:error], 'Please Login first !'
  end

  def test_new
    login_as('admin@admin.com')
    get :new
    assert_response :success
		assert_not_nil assigns(:category)
		assert_not_nil assigns(:select_category)
  end
  
  def test_create
    login_as('admin@admin.com')
    assert_difference('Category.count') do
      post :create, :category => {:name => 'electronic'}
      assert_not_nil assigns(:category)
      assert_equal assigns(:category).name, "electronic"
      assert_equal assigns(:category).valid?, true
    end
    assert_response :redirect
    assert_redirected_to admin_categories_path
    assert_equal flash[:notice], 'New Category Added ... '
  end
  
  def test_create_with_invalid_parameter
    login_as('admin@admin.com')
    assert_no_difference('Category.count') do
      post :create, :category => {:name => nil}
      assert_not_nil assigns(:category)
      assert_not_nil assigns(:select_category)
      assert_equal assigns(:category).valid?, false
    end
    assert_response :success
  end

  def test_edit
    login_as('admin@admin.com')
    get :edit, :id => Category.first.id
    assert_not_nil assigns(:category)
    assert_response :success
  end
  
  def test_update
    login_as('admin@admin.com')
    put :update, :id => Category.first.id,
                 :category => {:name => 'updated name'}
    assert_not_nil assigns(:category)
    assert_equal assigns(:category).name, 'updated name'
    assert_response :redirect
    assert_redirected_to admin_category_path(assigns(:category))
    assert_equal flash[:notice], 'Category Updated !' 
  end
  
  def test_update_with_invalid_parameter
    login_as('admin@admin.com')
    put :update, :id => Category.first.id,
                 :category => {:name => nil}
    assert_not_nil assigns(:category)
    assert_response :success
  end
  
  def test_destroy
    login_as('admin@admin.com')
    assert_difference('Category.count', -1) do
  	  delete :destroy, :id => 4
      assert_not_nil assigns(:category)
    end
    assert_response :redirect
    assert_redirected_to admin_categories_path
  end

  def test_destroy_parent_with_child
    login_as('admin@admin.com')
    assert_no_difference('Category.count', -1) do
      delete :destroy, :id => Category.first.id
      assert_not_nil assigns(:category)
    end
    assert_response :redirect
    assert_redirected_to admin_categories_path
    assert_equal flash[:notice], 'Category still have any child categories...' 
  end
end
