require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  def test_create
    login_as('endah@gmail.com')
    assert_difference('Comment.count') do
      post :create, :comment => {:comment => "new content", :article_id => Article.first.id, :user_id => session[:user_id]}
      assert_not_nil assigns(:comment)
      assert_equal assigns(:comment).comment, "new content"
      assert_equal assigns(:comment).valid?, true
    end
    assert_response :redirect
    assert_redirected_to article_path(Article.first.id)
    assert_equal flash[:notice], 'Comment was successfully created.'
  end

end
