require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  def setup
		@user = User.find(:first)
  end

  def test_index
		get :index
    assert_response :success
		assert_not_nil assigns(:products)
		assert_not_nil assigns(:articles)
  end

  def test_new
		get :new
    assert_response :success
		assert_not_nil assigns(:user)
  end

  def test_create
    assert_difference('User.count') do
      post :create, :user => {:email => "fghj@gmail.com", :password => "endah", :name => "endah"}
      assert_not_nil assigns(:user)
      assert_equal assigns(:user).name, "endah"
      assert_equal assigns(:user).valid?, true
    end
    assert_response :redirect
    assert_redirected_to user_path(assigns(:user))
    assert_equal flash[:notice], 'Signed up !'
  end

  def test_create_with_invalid_parameter
    assert_no_difference('User.count') do
      post :create, :user => {:name => nil}
      assert_not_nil assigns(:user)
      assert_equal assigns(:user).valid?, false
    end
    assert_response :success
  end

  def test_show
    login_as('admin@admin.com')
    get :show, :id => User.first.id
    assert_not_nil assigns(:user)
    assert_response :success
  end

  def test_edit
    login_as('admin@admin.com')
    get :edit, :id => User.first.id
    assert_not_nil assigns(:user)
    assert_response :success
  end
  
  def test_update
    login_as('admin@admin.com')
    put :update, :id => User.first.id,
                 :user => { :email => 'admin@admin.com', :password => "admin", :name => "Mr. Admin"}
		assert_not_nil assigns(:user)
    assert_equal assigns(:user).name, 'Mr. Admin'
    assert_response :redirect
    assert_redirected_to user_path(assigns(:user))
    assert_equal flash[:notice], 'User Updated !'
  end
  
  def test_update_with_invalid_parameter
    login_as('admin@admin.com')
    put :update, :id => User.first.id,
                 :user => {:name => nil}
    assert_not_nil assigns(:user)
    assert_response :success
  end
end
