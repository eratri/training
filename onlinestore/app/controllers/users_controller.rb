class UsersController < ApplicationController
	before_filter :require_login, :only => [:edit, :update, :show]
  before_filter :find_user_by_id, :only => [:edit, :update, :show]

	#homepage
	def index
		#@products = Product.find_by_sql("SELECT * FROM `products` ORDER BY `updated_at` DESC LIMIT 6 " )
		#@articles = Article.find_by_sql("SELECT * FROM `articles` ORDER BY `updated_at` DESC LIMIT 3 " ) 
		@products = Product.last(6).reverse
		@articles = Article.last(3).reverse
	end

	#form sign up
	def new
		@user = User.new
  end

	#process sign up
	def create
    @user = User.new(params[:user])
    if verify_recaptcha
      if @user.save
        UserMailer.registration_confirmation(@user).deliver 
				redirect_to(user_path(@user), :notice => "Signed up !")
      else
        render "new"
      end
    else
       flash[:error] = "There was an error with the recaptcha code below. #Please re-enter the code and click submit."
       render "new"
    end
  end

	#show user profile
	def show
  end
	
	#edit user
	def edit
	end
  
	#process update data user
  def update
   if @user.update_attributes(params[:user])
     flash[:notice] = 'User Updated !'
     redirect_to user_path(@user)
   else
     render :action => :edit
   end
  end

	#find user_id
	def find_user_by_id
    @user = User.find(params[:id])
  end
end
