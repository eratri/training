class CommentsController < ApplicationController
	before_filter :require_login, :only => [:new, :create]

	#process create comment
  def create
    @comment = Comment.new(params[:comment]) 
    respond_to do |format|
      if @comment.save
        format.html { redirect_to(article_path(@comment.article_id), :notice => 'Comment was successfully created.') }
      format.js
      end
    end
  end
end
