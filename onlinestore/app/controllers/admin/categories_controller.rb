class Admin::CategoriesController < Admin::ApplicationController
	before_filter :require_admin_login
	before_filter :find_article_by_id, :only => [:edit, :update, :destroy, :show]
  
	#listing category
	def index
    @categories = Category.find_by_sql("SELECT c.id as 'id', c.name AS 'name', p.name AS 'parent_name' FROM categories AS c LEFT OUTER JOIN categories AS p ON p.id = c.parent_id Order by p.name")
	end

	# form new category
	def new
    @category = Category.new
		@select_category = Category.where(["parent_id IS NULL"]).map{|x| [x.name, x.id]}
  end

	#Process save new category
  def create
    @category = Category.new(params[:category]) 
    if @category.save 
      flash[:notice] = 'New Category Added ... ' 
      redirect_to admin_categories_path
    else 
			@select_category = Category.where(["parent_id IS NULL"]).map{|x| [x.name, x.id]}
      render "new"
    end  
  end

	#form edit category
	def edit
		@select_category = Category.where(["parent_id IS NULL"]).map{|x| [x.name, x.id]}
	end

	#show detail category
	def show
	end
  
	#process update category
  def update
  	if @category.update_attributes(params[:category])
      flash[:notice] = 'Category Updated !' 
      redirect_to admin_category_path(@category)
    else
			@select_category = Category.where(["parent_id IS NULL"]).map{|x| [x.name, x.id]}
      render "edit"
    end
  end

	#delete category
  def destroy
		@find_children = Category.where('parent_id = ? ', @category)
		if @find_children.empty?
	    @category.destroy
	    redirect_to admin_categories_path
		else
			flash[:notice] = 'Category still have any child categories...' 
		  redirect_to admin_categories_path
		end
  end

	#find article by id
  def find_article_by_id
    @category = Category.find(params[:id])
  end
end
