 class Admin::ApplicationController < ApplicationController
  protect_from_forgery

  #method authentication admin
  def require_admin_login
    if current_user.nil?
      flash[:error] = "Please Login first !"
      redirect_to log_in_path
    else
      if current_user.email == 'admin@admin.com'
	      return current_user
      else
	      flash[:error] = "Only admins are permitted"
	      redirect_to root_url
			end
    end
  end
  
	#get current user
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id] 
  end
end
