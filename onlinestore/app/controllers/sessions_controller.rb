class SessionsController < ApplicationController
  layout 'login'
	
	#create form log_in
  def new
    
  end

	#process log_in
  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      session[:email] = user.email
      redirect_to user_path(user.id), :notice => "Logged in!"
    else
      flash.now.alert = "Invalid email or password" 
      render "new"
    end
  end
  
	#process log_out
  def destroy
    session[:user_id] = nil
    redirect_to log_in_path, :notice => "Logged out!"
  end 
end
