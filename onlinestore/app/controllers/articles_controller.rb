class ArticlesController < ApplicationController
  before_filter :require_login, :only => [:new, :edit, :update]
  before_filter :find_article_by_id, :only => [:edit, :update, :show, :destroy]
  before_filter :authenticate_edit, :only => [:edit, :update, :destroy]

	#listing article
  def index
    @articles = Article.order('updated_at DESC')
  end
  
	#form new article
  def new
    @article = Article.new
  end

	#process save new article
  def create
    @article = Article.new(params[:article]) 
    if @article.save 
      flash[:notice] = 'New Article Added ... ' 
      redirect_to articles_path 
    else 
      render "new" 
    end  
  end
  
	#form edit article
  def edit; end
  
	#process update article
  def update
   if @article.update_attributes(params[:article])
     flash[:notice] = 'Article Updated !' 
     redirect_to article_path(@article)
   else
     render :action => :edit
   end
  end

	#delete article
  def destroy
    @article.destroy
    redirect_to articles_path
  end

	#show detail an article
  def show
    @comments = @article.comments
    @comment = Comment.new
  end
 
	#find selected article
  def find_article_by_id
    @article = Article.find(params[:id])
  end

	#edit/delete user authentication
  def authenticate_edit
	    if session[:user_id] != @article.user_id
       flash[:notice] = 'You are not authenticated user ... '
       redirect_to :action => :index
  		end
  end
end
