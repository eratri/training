class ProductsController < ApplicationController
	before_filter :require_login, :only => [:new, :create]
  before_filter :find_product_by_id, :only => [:edit, :update, :show, :destroy]
  before_filter :authenticate_edit, :only => [:edit, :update, :destroy]

	#listing all product
  def index
    @products = Product.order('updated_at DESC')
  end

	#form add new product
  def new
    @category = Category.order('parent_id')
    @product = Product.new
  end

	#process save new product
  def create
    @product = Product.new(params[:product]) 
    if @product.save 
      flash[:notice] = 'New Product Added ... ' 
      redirect_to products_path 
    else 
	    @category = Category.order('parent_id')
      render new_product_path 
    end  
  end
  
  #form edit product
  def edit
	    @category = Category.order('parent_id')
 	end
  
  #process update
  def update
   if @product.update_attributes(params[:product])
     flash[:notice] = 'Product Updated !' 
     redirect_to product_path(@product)
   else
	    @category = Category.order('parent_id')
     render :action => :edit
   end
  end

	#delete product
  def destroy
    @product.destroy
    redirect_to products_path
  end

	#show detail a product
  def show
		@category = @product.category
  end
 
	#listing by category(for homepage user use)
  def list
    if params[:id].nil?
	    redirect_to products_path
		else
	    @category = Category.find(params[:id])
			@products = @category.products
		end
  end

	#find selected product
  def find_product_by_id
    @product = Product.find(params[:id])
  end

	#authentication edit
  def authenticate_edit
	    if session[:user_id] != @product.user_id
       flash[:notice] = 'You are not authenticated user ... '
       redirect_to :action => :index
  		end
  end
end
