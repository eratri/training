class Category < ActiveRecord::Base
	has_many :products, :dependent => :destroy
  validates :name, :presence => true, :uniqueness => true

	#method to show category in select option list
	def category_tree
		if parent_id.nil?
    	"#{name}"
		else
			parent = Category.find_by_id(parent_id)
			"__ #{parent.name} : #{name}"
		end
  end

end
