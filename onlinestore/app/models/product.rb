class Product < ActiveRecord::Base
	belongs_to :user
  belongs_to :category

  validates :name, :presence => true
	validates :price, :presence => true, :numericality => true
  validates :category_id, :presence => true
end
