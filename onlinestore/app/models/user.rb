class User < ActiveRecord::Base
	has_many :articles, :dependent => :destroy
	has_many :comments, :dependent => :destroy
	has_many :products, :dependent => :destroy

	attr_accessor :password
  before_save :encrypt_password

  #validation encrypt password
  def encrypt_password
    if password.present?
          self.password_salt = BCrypt::Engine.generate_salt
          self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
     end
  end
 
	validates :password, :presence => {:on => :create}, :confirmation => true
	validates :email, :presence => true, :uniqueness => true, :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
	validates :name, :presence => true

  #method authentication login
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
end
